# CSCIFYP-ImgToGDS

This is a simple python script to convert images to GDS rectangles.

Requirements:
* Python 3.7
* tkinter in Python 3.7
* gdspy
* matplotlib
* numpy
