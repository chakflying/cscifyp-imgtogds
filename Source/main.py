import os
import time
import tkinter

import gdspy
import imagehash
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageOps

SCALE = 4

# Function to convert image file to gds rectangles. Takes in variable trace width by number of pixels.


def imgToGds(file_path, trace_width_px, show_result):
    # Load image file and adding padding
    filename = os.path.basename(file_path).split('.')[0]
    img1 = Image.open(file_path)
    img1 = img1.convert('L')
    ihash = imagehash.average_hash(img1, hash_size=16)
    padding = (1, 1, 1, 1)
    img1 = ImageOps.expand(img1, padding)
    pix1 = np.array(img1)
    if show_result:
        plt.imshow(pix1)
        plt.show()

    # Normalize to [0,1] values only
    for x in np.nditer(pix1, op_flags=['readwrite'], flags=['external_loop']):
        x[...] = x / 255

    polygons = []
    polygons_type = []
    # Iterate line by line to remove rectangles by the longest first principle
    it = np.nditer(pix1, flags=['multi_index'], op_flags=['readonly'])
    with it:
        while not it.finished:
            if it[0] == 1:
                NewPolygon = [(it.multi_index[0], it.multi_index[1])]
                starty, startx = it.multi_index[0], it.multi_index[1]
                x, y = startx, starty
                while True:
                    if pix1[starty, x + 1] != 1:
                        break
                    x += 1
                while True:
                    if pix1[y + 1, startx] != 1:
                        break
                    y += 1

                if x - startx > y - starty:
                    # This rectangle is tall
                    polygons_type.append(2)
                    NewPolygon.append((starty+trace_width_px-1, x))
                    for clearx in range(startx, x + 1):
                        for width in range(trace_width_px):
                            pix1[starty + width, clearx] = 0
                else:
                    # This rectangle is wide
                    polygons_type.append(1)
                    NewPolygon.append((y, startx+trace_width_px-1))
                    for cleary in range(starty, y + 1):
                        for width in range(trace_width_px):
                            pix1[cleary, startx+width] = 0

                polygons.append(NewPolygon)
            it.iternext()

    # Put rectangles to GDS layer according to scale
    lib = gdspy.GdsLibrary(name=f'{filename}-{int(time.time())}')
    cell = gdspy.Cell(f'lithogen-{filename}-{int(time.time())}')
    overlay = gdspy.Cell(f'overlay-{filename}-{int(time.time())}')


    index = 0
    for polygon in polygons:
        rect = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * polygon[0][1] - SCALE, SCALE - 1 + SCALE * 2 * polygon[0]
                                [0] - SCALE), (SCALE - 1 + SCALE * 2 * polygon[1][1] + SCALE, SCALE - 1 + SCALE * 2 * polygon[1][0] + SCALE))
        cell.add(rect)
        if polygons_type[index] == 1:
            point1 = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * (polygon[0][1]) - SCALE / 2, SCALE - 1 + SCALE * 2 * polygon[0][0] - SCALE / 2), (
                SCALE - 1 + SCALE * 2 * (polygon[0][1]) + SCALE / 2, SCALE - 1 + SCALE * 2 * polygon[0][0] + SCALE / 2))
            point11 = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * (polygon[0][1] + trace_width_px) - SCALE / 2, SCALE - 1 + SCALE * 2 * polygon[0][0] - SCALE / 2), (
                SCALE - 1 + SCALE * 2 * (polygon[0][1] + trace_width_px) + SCALE / 2, SCALE - 1 + SCALE * 2 * polygon[0][0] + SCALE / 2))
            point2 = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * (polygon[1][1] + trace_width_px / 2) - SCALE / 2, SCALE - 1 + SCALE * 2 * polygon[1][0] - SCALE / 2), (
                SCALE - 1 + SCALE * 2 * (polygon[1][1] + trace_width_px / 2) + SCALE / 2, SCALE - 1 + SCALE * 2 * polygon[1][0] + SCALE / 2))
            point21 = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * (polygon[1][1] - trace_width_px / 2) - SCALE / 2, SCALE - 1 + SCALE * 2 * polygon[1][0] - SCALE / 2), (
                SCALE - 1 + SCALE * 2 * (polygon[1][1] - trace_width_px / 2) + SCALE / 2, SCALE - 1 + SCALE * 2 * polygon[1][0] + SCALE / 2))
        elif polygons_type[index] == 2:
            point1 = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * polygon[0][1] - SCALE/2, SCALE - 1 + SCALE * 2 * (
                polygon[0][0] + trace_width_px / 2) - SCALE / 2), (SCALE - 1 + SCALE * 2 * polygon[0][1] + SCALE / 2, SCALE - 1 + SCALE * 2 * (polygon[0][0] + trace_width_px / 2) + SCALE/2))
            point11 = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * polygon[0][1] - SCALE/2, SCALE - 1 + SCALE * 2 * (
                polygon[0][0] - trace_width_px / 2) - SCALE / 2), (SCALE - 1 + SCALE * 2 * polygon[0][1] + SCALE / 2, SCALE - 1 + SCALE * 2 * (polygon[0][0] - trace_width_px / 2) + SCALE/2))
            point2 = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * polygon[1][1] - SCALE / 2, SCALE - 1 + SCALE * 2 * (
                polygon[1][0]) - SCALE / 2), (SCALE - 1 + SCALE * 2 * polygon[1][1] + SCALE / 2, SCALE - 1 + SCALE * 2 * (polygon[1][0]) + SCALE / 2))
            point21 = gdspy.Rectangle((SCALE - 1 + SCALE * 2 * polygon[1][1] - SCALE / 2, SCALE - 1 + SCALE * 2 * (
                polygon[1][0] - trace_width_px) - SCALE / 2), (SCALE - 1 + SCALE * 2 * polygon[1][1] + SCALE / 2, SCALE - 1 + SCALE * 2 * (polygon[1][0] - trace_width_px) + SCALE / 2))
        overlay.add(point1)
        overlay.add(point11)
        overlay.add(point2)
        overlay.add(point21)
        index += 1

    if show_result:
        gdspy.LayoutViewer(library=lib, cells=[cell, overlay])
    lib.write_gds(f'Output/{filename}.gds', [cell, overlay])
    return (index, ihash)


for i in range(100):
    (no_rectangles, ihash) = imgToGds(
        f'Samples/batch/1 Litho_02 {i}.png', trace_width_px=2, show_result=False)
    print(f'Sample {i} image hash: {ihash}')
